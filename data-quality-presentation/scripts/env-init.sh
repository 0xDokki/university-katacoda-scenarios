#!/usr/bin/bash

docker run -d -e POSTGRES_PASSWORD=postgres --name postgres postgres:12.4
sleep 5s

wget https://gitlab.com/0xDiddi/university-katacoda-scenarios/-/raw/master/university-data-quality/scripts/create-users-1.sql
wget https://gitlab.com/0xDiddi/university-katacoda-scenarios/-/raw/master/university-data-quality/scripts/create-sales.sql
wget https://gitlab.com/0xDiddi/university-katacoda-scenarios/-/raw/master/university-data-quality/scripts/create-orders.sql

docker exec -i -u postgres postgres psql < ./create-users-1.sql
docker exec -i -u postgres postgres psql < ./create-sales.sql
docker exec -i -u postgres postgres psql < ./create-orders.sql

# clear the terminal and enter an interactive postgres prompt, leaving a clean environment for the user

clear
docker exec -it -u postgres postgres psql
