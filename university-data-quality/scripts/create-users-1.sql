CREATE TABLE users (
    user_id integer,
    name varchar(100),
    age integer,
    address varchar(100),
    hobbies varchar(100),

    primary key (user_id)
);

INSERT INTO users (user_id,name,age,address, hobbies) VALUES
 (0,'Simon Hoffmann',27,'Mühlenweg 34, 44135 Dortmund','video games,woodworking')
,(1,'Arno Müller',29,'Lange Straße 33, 45127 Essen','bowling,card games,cooking')
,(2,'Max Ackner',40,'Dortmunder Weg 17, 50667 Köln','metalworking,cooking')
,(3,'Jens Müller',48,'Kirchweg 31, 44135 Dortmund','fishing,chess')
,(4,'Simon Winter',46,'Mühlenweg 92, 33602 Bielefeld','meditation,tabletop games')
,(5,'Simon Ackner',45,'Düsseldorfer Straße 52, 50667 Köln','fishing,woodworking')
,(6,'Moritz Meier',27,'Dortmunder Straße 4, 40210 Düsseldorf','diving,photography')
,(7,'Moritz Eichmann',30,'Hauptstraße 83, 45127 Essen','metalworking,chess')
,(8,'Stefan Hoffmann',48,'Bachstraße 55, 33602 Bielefeld','basketball,meditation')
,(9,'Jens Ackner',23,'Kirchweg 85, 33602 Bielefeld','video games,tabletop games');
