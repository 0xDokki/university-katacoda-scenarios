CREATE TABLE sales (
   date       DATE
  ,product_id INTEGER
  ,name       VARCHAR(15)
  ,price      INTEGER
  ,num_sold   INTEGER

  ,PRIMARY KEY(date,product_id)
);

INSERT INTO sales(date,product_id,name,price,num_sold) VALUES
 ('2021-03-20',0,'bread',5,24)
,('2021-03-21',0,'bread',5,15)
,('2021-03-21',1,'cookies',2,60)
,('2021-03-22',0,'bread',5,26)
,('2021-03-22',2,'butter',3,5)
,('2021-03-23',1,'cookies',2,55)
,('2021-03-23',3,'soda',5,5)
,('2021-03-24',1,'cookies',2,57)
,('2021-03-24',2,'butter',3,6)
,('2021-03-24',3,'soda',5,15);
