This katacoda aims to demonstrate database normalization levels,
and the impact they have on data quality.

We'll be looking at a few practical examples,
seeing how the data looks before normalization, what problems this may cause,
and what steps are necessary to normalize the data.
