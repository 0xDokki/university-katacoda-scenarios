import random

NUM_ENTRIES = 10

# Names by https://www.fantasynamegenerators.com/
first_names = [
    "Max",
    "Moritz",
    "Arno",
    "Simon",
    "Stefan",
    "Jens",
    "Andreas"
]

last_names = [
    "Müller",
    "Meier",
    "Winter",
    "Hoffmann",
    "Ackner",
    "Eichmann",
    "Naumann"
]

street_names = [
    "Hauptstraße",
    "Kirchweg",
    "Bachstraße",
    "Mühlenweg",
    "Lange Straße",
    "Ruhrallee",
    "Dortmunder Weg",
    "Düsseldorfer Straße"
]

city_names = [
    ("Dortmund", "44135"),
    ("Essen", "45127"),
    ("Düsseldorf", "40210"),
    ("Köln", "50667"),
    ("Bielefeld", "33602")
]

users = "user_id;name;age;address\n"

for i in range(NUM_ENTRIES):
    city = random.choice(city_names)

    u = "{};{} {};{};{} {}, {} {}\n".format(
        i,
        random.choice(first_names),
        random.choice(last_names),
        random.randint(20,50),
        random.choice(street_names),
        random.randint(1,100),
        city[1], city[0]
    )
    users += u

print(users)


shipping_method = [
    "same day",
    "next day",
    "regular"
]

orders = "user_id;first_name;last_name;age;street;zip;city;order_id;cost;shipping_method;num_items\n"

for i in range(NUM_ENTRIES):
    city = random.choice(city_names)

    o = ";{};{};{};{}\n".format(
        i+NUM_ENTRIES,
        random.randint(10,75),
        random.choice(shipping_method),
        random.randint(1,6)
    )
    orders += o

print(orders)